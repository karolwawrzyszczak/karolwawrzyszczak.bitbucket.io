aktualny=2;
max_slajd=3;
$(document).ready(function () {
    slider();
});

function slider() {
    poprz=$(".prev");
    nast=$(".next");
    slides=$(".slide");

    nast.click(function () {
        wyswietlSlajd(aktualny);
        if(aktualny==max_slajd){
            aktualny=1;
        }else{
            aktualny=aktualny+1;
        }

    })
    poprz.click(function () {
        wyswietlSlajd(aktualny);
       
        aktualny=aktualny-1;
		if(aktualny==0){aktualny=max_slajd;};
		
    })

}

function wyswietlSlajd(aktualny) {
    sliderco=$("#slider #slides");
    sliderco.fadeOut(100, function () {
        slides.removeClass("active") ;
        slides.eq(aktualny).addClass("active");
        sliderco.fadeIn(100);
    })
}