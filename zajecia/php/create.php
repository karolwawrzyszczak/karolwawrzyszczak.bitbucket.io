<?php 
     require 'database.php';
      if ( !empty($_POST)) {
        // inicjalizowanie wartości zmiennych kontroli poprawnosci wprowadzania 
		$indeksError = null;
		$imieError = null;
        $nazwiskoError = null;
        $mobileError = null;
        $emailError = null;
        $adresError = null;

        // wartości tablicy POST
		$indeks = $_POST['indeks'];
		$imie = $_POST['imie'];
        $nazwisko = $_POST['nazwisko'];
        $email = $_POST['email'];
        $mobile = $_POST['mobile'];
        $adres = $_POST['adres'];
        
        // walidacja kolejnych zmiennych pól formularza
        $valid = true;
        if (empty($indeks)) {
            $indeksError = 'wprowadź numer indeksu';
            $valid = false;
        }
         if (empty($imie)) {
          $imieError = 'wprowadź imie studenta';
            $valid = false;
        }
		if (empty($nazwisko)) {
           $nazwiskoError = 'wprowadź nazwisko studenta';
            $valid = false;
        }		
		if (empty($email)) {
            $emailError = 'Wprowadź adress email';
            $valid = false;
        } else if ( !filter_var($email,FILTER_VALIDATE_EMAIL) ) {
            $emailError = 'wprowadź poprawny adres email';
            $valid = false;
        }
         if (empty($mobile)) {
            $mobileError = 'wprowadź numer telefonu';
            $valid = false;
        }
        if (empty($adres)) {
           $adresError = 'wprowadź adres';
           $valid = false;
       }
		// wprowadź dane
        if ($valid) {
			echo "ok- wprowadzenie";
            $pdo = Database::connect();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "INSERT INTO dane (indeks,imie,nazwisko,email,mobile,adres) values(?,?,?,?,?,?)";
            $q = $pdo->prepare($sql);
            $q->execute(array($indeks, $imie, $nazwisko, $email, $mobile, $adres));
            Database::disconnect();
            header("Location: index.php");
        }
    }
?>
<!DOCTYPE html>
<html lang="pl">

<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>

</head>
<body>
    <div class="container">
	<div class="span10 offset1">
        <div class="row">
            <h3>Nowy student</h3>
        </div>
              
        <form  action="create.php" method="post">
            <div class="form-group row" >
                <label  class="col-sm-1 control-label">Indeks</label>
			    <div class="col-sm-5">
                  <input name="indeks" type="text"  class="form-control" placeholder="wpisz indeks studenta" value="<?php echo !empty($indeks)?$indeks:'';?>" >
				  <?php if (!empty($indeksError)): ?>
                  <span class="text-danger"><?php echo $indeksError;?></span>
                  <?php endif; ?>

			    </div>
            </div>
             <div class="form-group row">
                <label class="col-sm-1 control-label">Imię</label>
			    <div class="col-sm-5">
                  <input name="imie" type="text"  class="form-control" placeholder="wpisz imię studenta" value="<?php echo !empty($imie)?$imie:'';?>">
				  <?php if (!empty($imieError)): ?>
                                <span class="text-danger"><?php echo $imieError;?> </span>
                  <?php endif; ?>
			    </div>
            </div>   
            <div class="form-group row">
                <label class=" col-sm-1 control-label">Nazwisko</label>
			    <div class="col-sm-5">
                  <input name="nazwisko" type="text"  class="form-control" placeholder="wpisz nazwisko studenta" value="<?php echo !empty($nazwisko)?$nazwisko:'';?>">
				  <?php if (!empty($nazwiskoError)): ?>
                                <span class="text-danger"><?php echo $nazwiskoError;?> </span>
                  <?php endif; ?>
			    </div>
            </div>  
            <div class="form-group row">
                <label class=" col-sm-1 control-label">E-mail</label>
			    <div class="col-sm-5">
                  <input name="email" type="text"  class="form-control" placeholder="wpisz e-mail studenta" value="<?php echo !empty($email)?$email:'';?>">
				  <?php if (!empty($emailError)): ?>
                                <span class="text-danger"><?php echo $emailError;?> </span>
                  <?php endif; ?>
			    </div>
            </div>  
            <div class="form-group row">
                <label class=" col-sm-1 control-label">Telefon</label>
			    <div class="col-sm-5">
                  <input name="mobile" type="text"  class="form-control" placeholder="wpisz telefon studenta" value="<?php echo !empty($mobile)?$mobile:'';?>">
				  <?php if (!empty($mobileError)): ?>
                                <span class="text-danger"><?php echo $mobileError;?> </span>
                  <?php endif; ?>
			    </div>
            </div>  			
            <div class="form-group row">
                <label class=" col-sm-1 control-label">Adres</label>
                <div class="col-sm-5">
                  <input name="adres" type="text"  class="form-control" placeholder="wpisz adres studenta" value="<?php echo !empty($adres)?$adres:'';?>">
                  <?php if (!empty($adresError)): ?>
                                <span class="text-danger"><?php echo $adresError;?> </span>
                  <?php endif; ?>
                </div>
            </div>
            <div class="form-actions">
                <button type="submit" class="btn btn-success">Zapisz</button>
                <a class="btn btn-primary" href="index.php">Cofnij</a>
            </div>
        </form>
      </div>           
    </div> <!-- /container -->
  </body>

</html>