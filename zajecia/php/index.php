<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container"> 
 <div class="row">
   <h3>PHP CRUD dla tabeli crudStudent</h3>
 </div>
 <div class="row">
 <p>
  <a href="create.php" class="btn btn-success">Utwórz</a>
 </p>

 <table class="table table-striped table-bordered">
    <thead>
      <tr>
        <th>id</th>
        <th>indeks</th>
		<th>Imie</th>
		<th>Nazwisko</th>
        <th>Adres e-mail</th>
        <th>Numer telefonu</th>
        <th>Adres</th>
       </tr>
      </thead>
      <tbody>
	  <?php
           include 'database.php';
           $pdo = Database::connect();
           $sql = 'SELECT * FROM dane ';
           foreach ($pdo->query($sql) as $row) {
                    echo '<tr>';
					echo '<td>'. $row['id'] . '</td>';
			    	echo '<td>'. $row['indeks'] . '</td>';
                    echo '<td>'. $row['imie'] . '</td>';
					echo '<td>'. $row['nazwisko'] . '</td>';
                    echo '<td>'. $row['email'] . '</td>';
                    echo '<td>'. $row['mobile'] . '</td>';
                    echo '<td>'. $row['adres'] . '</td>';
					echo '<td><a class="btn btn-secondary" href="read.php?indeks='.$row['indeks'].'">Przeglądaj</a></td>';
					echo '<td><a class="btn btn-success" href="update.php?indeks='.$row['indeks'].'">Aktualizuj</a></td>';
					echo '<td><a class="btn btn-danger" href="delete.php?indeks='.$row['indeks'].'">Usuń</a></td>';
                    echo '</tr>';
           }
           Database::disconnect();
?>

      </tbody>
 </table>
 </div>
 </div> <!-- /container -->
 </body>
</html>