<?php 
    require 'database.php';
 
    $indeks = null;
    if ( !empty($_GET['indeks'])) {
        $indeks = $_REQUEST['indeks'];
    }

     
    if ( null==$indeks ) {
        header("Location: index.php");
    }
  
  if ( !empty($_POST)) {
    // ustaw zmienne dla bԥd󷠰rzy sprawdzaniu poprawnosci 
    $imieError = null;
    $nazwiskoError = null;
    $emailError = null;
    $mobileError = null;
    $adresError = null;

    // ustaw zmienne pobrane z tablicy $_POST
    $imie = $_POST['imie'];
    $nazwisko = $_POST['nazwisko'];
    $email = $_POST['email'];
    $mobile = $_POST['mobile'];
    $adres = $_POST['adres'];

    // walidacja danych
    $valid = true;
    if (empty($imie)) {
      $imieError = 'wprowadź imie studenta';
      $valid = false;
    }
		if (empty($nazwisko)) {
      $nazwiskoError= 'wprowadź nazwisko studenta';
      $valid = false;
    }    

    if (empty($email)) {
      $emailError = 'wprowadź adres email studenta';
      $valid = false;
    } else if ( !filter_var($email,FILTER_VALIDATE_EMAIL) ) {
      $emailError = 'wprowadź poprawny adres email studenta';
      $valid = false;
    }
    
    if (empty($mobile)) {
      $mobileError = 'Please enter Mobile Number';
      $valid = false;
    }
   if (empty($adres)) {
     $adresError = 'Please enter Adres Number';
     $valid = false;
   }
    // aktualizuj dane
    if ($valid) {
            $pdo = Database::connect();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "UPDATE dane SET imie = ?, nazwisko = ?, email = ?, mobile = ?, adres = ? WHERE indeks = ?";
            $q = $pdo->prepare($sql);
            $q->execute(array($imie,$nazwisko,$email,$mobile,$adres,$indeks));
            Database::disconnect();
            header("Location: index.php");
        }
    } else {
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "SELECT * FROM dane where indeks = ?";
        $q = $pdo->prepare($sql);
        $q->execute(array($indeks));
        $data = $q->fetch(PDO::FETCH_ASSOC);
        $imie = $data['imie'];
        $nazwisko = $data['nazwisko'];
        $email = $data['email'];
        $mobile = $data['mobile'];
        $adres = $data['adres'];
        Database::disconnect();
    }
?>

<!DOCTYPE html>
<html lang="pl">

<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>

</head>
<body>
    <div class="container">
	<div class="span10 offset1">
        <div class="row">
            <h3>Aktualizacja danych studenta</h3>
        </div>
              
        <form  action="update.php?indeks=<?php echo $indeks?>" method="post">
            <div class="form-group row" >
                <label class="col-sm-1 control-label">Imie</label>
                    <div class="col-sm-5">
                        <input name="imie" type="text"  class="form-control" placeholder="imie" value="<?php echo !empty($imie)?$imie:'';?>">
                            <?php if (!empty($imieError)): ?>
                                <span class="text-danger"><?php echo $imieError;?></span>
                            <?php endif; ?>
                     </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-1 control-label">Nazwisko</label>
                    <div class="col-sm-5">
                            <input name="nazwisko" type="text" class="form-control"  placeholder="nazwisko" value="<?php echo !empty($nazwisko)?$nazwisko:'';?>">
                            <?php if (!empty($nazwiskoError)): ?>
                                <span class="text-danger"><?php echo $nazwiskoError;?></span>
                            <?php endif; ?>
                        </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-1 control-label">E-mail</label>
                    <div class="col-sm-5">
                            <input name="email" type="text" class="form-control" placeholder="email" value="<?php echo !empty($email)?$email:'';?>">
                            <?php if (!empty($emailError)): ?>
                                <span class="text-danger"><?php echo $emailError;?></span>
                            <?php endif;?>
                    </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-1 control-label">Telefon</label>
                    <div class="col-sm-5">
                            <input name="mobile" type="text"  class="form-control" placeholder="mobile" value="<?php echo !empty($mobile)?$mobile:'';?>">
                            <?php if (!empty($mobileError)): ?>
                                <span class="text-danger"><?php echo $mobileError;?></span>
                            <?php endif;?>
                    </div>
            </div>
           <div class="form-group row">
               <label class="col-sm-1 control-label">Adres</label>
                   <div class="col-sm-5">
                           <input name="adres" type="text"  class="form-control" placeholder="adres" value="<?php echo !empty($adres)?$adres:'';?>">
                           <?php if (!empty($adresError)): ?>
                               <span class="text-danger"><?php echo $adresError;?></span>
                           <?php endif;?>
                   </div>
           </div>
            <div class="form-actions">
                <button type="submit" class="btn btn-success">Zapisz</button>
                <a class="btn btn-primary" href="index.php">Cofnij</a>
            </div>
        </form>
      </div>           
    </div> <!-- /container -->
	
  </body>

</html>