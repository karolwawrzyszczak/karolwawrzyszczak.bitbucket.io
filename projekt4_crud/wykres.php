<html lang="pl">
<head>
    <meta charset="UTF-8">
    <title>Wykres</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <?php SESSION_START();?>
<?php
             require 'database.php';
                 $pdo = Database::connect();
                 //$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                 //$pdo = mysqli_query("SELECT COUNT(*) as idd FROM samochodziki");

                //liczy ilosc tabel
                $sql=$pdo->query("SELECT COUNT(*) FROM samochodziki");
                $row=$sql->fetch();
                $amount=$row[0];

                //liczy ilosc samochodzikow
                for($i=1;$i<$amount+1;$i++){
                    $sql=$pdo->query("SELECT COUNT(*) FROM samochodziki WHERE id_dziecka= '".$i."'");
                    $row=$sql->fetch();
                    $count[$i] =$row[0];

                    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    $sql = "SELECT * FROM dzieci where id = ?";
                    $q = $pdo->prepare($sql);
                    $q->execute(array($i));
                    $data = $q->fetch(PDO::FETCH_ASSOC);

                    $imiona[$i] = $data['imie'];
                    $nazwiska[$i] = $data['nazwisko'];
                    $id_dziecka[$i] = $data['id'];
                 }

        //pozbyc sie pustosci
        $k=1;
        for($i=1;$i<$amount;$i++){
            if($count[$i]!=0){
                $nimiona[$k]=$imiona[$i];
                $nnazwiska[$k]=$nazwiska[$i];
                $id_dziecka[$k]=$id_dziecka[$i];

                $ncount[$k]=$count[$i];
                $k++;
            }
        }

                 for($j=1;$j<$k-1;$j++){
                    for($i=1;$i<$k-1;$i++){
                      if($ncount[$i]!=0){
                          if($ncount[$i]>$ncount[$i+1]){
                            $old=$ncount[$i];
                            $ncount[$i]=$ncount[$i+1];
                            $ncount[$i+1]=$old;

                            $stare=$nimiona[$i];
                            $nimiona[$i]=$nimiona[$i+1];
                            $nimiona[$i+1]=$stare;

                            $stare2=$nnazwiska[$i];
                            $nnazwiska[$i]=$nnazwiska[$i+1];
                            $nnazwiska[$i+1]=$stare2;

                            $stare3=$id_dziecka[$i];
                            $id_dziecka[$i]=$id_dziecka[$i+1];
                            $id_dziecka[$i+1]=$stare3;
                          };
                      }
                  }}

                 Database::disconnect();

             ?>
     <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript">
          google.charts.load('current', {'packages':['bar']});
          google.charts.setOnLoadCallback(drawChart);

          function drawChart() {
            var data = google.visualization.arrayToDataTable([
              ['Dzieci', 'Samochodziki'],

          <?php
              for($i=1;$i<$k;$i++){
                    echo "['".$nimiona[$i]." ".$nnazwiska[$i]."' ,'".$ncount[$i]."'],";}
          ?>
            ]);

            var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

            chart.draw(data, null);
          }
        </script>
</head>
<body>
    <header><br>
        <div class="row justify-content-center">
            <a href="panel.php"><h1>---- COFNIJ -----</h1></a>
            </div>
    </header><br><br>

    <div class="container">
            <div class="row justify-content-center">
                <h3>Wykres posiadania samochodzików do dzieci</h3>
            </div>
            <div class="row justify-content-center">
                  <div id="columnchart_material" style="width: 1200px; height:200px;"></div>
            </div>

           <br><br>
           <div class="row justify-content-center">
                       <div class="row justify-content-center">
                           <h3>Raport z wynikiem liczbowym posiadania samochodzików</h3>
                       </div>

              <table class="table table-striped table-bordered">
                   <thead>
                   <tr>
                       <th>id dziecka</th>
                       <th>Imie Nazwisko</th>
                       <th>ilość aut</th>
                   </tr>
                   </thead>
                   <tbody>
               <?php

                   for($i=1;$i<$k;$i++){
                                     echo '<tr>';
                                       echo '<td>'. $id_dziecka[$i].'</td>';
                                       echo '<td>'. $nimiona[$i].' ' .$nnazwiska[$i] . '</td>';
                                       echo '<td>'. $ncount[$i] . '</td>';
                                       echo '</tr>';
                   }echo '<br><br>';
               ?>
                       </tbody>
                   </table>
               </div>

    </div>
</body>
</html>
