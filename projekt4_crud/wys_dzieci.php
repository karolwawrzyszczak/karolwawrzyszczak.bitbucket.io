<html lang="pl">
<head>
    <meta charset="UTF-8">
    <title>Baza dzieci</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <?php SESSION_START();?>

</head>
<body>
<header><br>
    <div class="row justify-content-center">
        <a href="panel.php"><h1>---- COFNIJ -----</h1></a>

    </div>
</header><br><br>

<div class="container">
    <div class="row">
        <h3>Lista dzieci</h3>
    </div>
    <p>
      <a href="lista_dzieci.php" class="btn btn-success">Edytuj bazę dziecko</a>
     </p>

    <div class="row">

        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>id</th>
                <th>imie</th>
                <th>nazwisko</th>
                <th>nr telefonu rodzica</th>
            </tr>
            </thead>
            <tbody>
            <?php
require 'database.php';
                 $pdo = Database::connect();
           $sql = 'SELECT * FROM dzieci ';
           foreach ($pdo->query($sql) as $row) {
            echo '<tr>';
                echo '<td><a href="dzieci/read.php?indeks='.$row['id'].'">'.$row['id'].'</a></td>';
                echo '<td>'. $row['imie'] . '</td>';
                echo '<td>'. $row['nazwisko'] . '</td>';
                echo '<td>'. $row['rodzic_telefon'] . '</td>';
                echo '</tr>';
            }
            //Database::disconnect();
            ?>

            </tbody>
        </table>
    </div>

    </body>
    </html>
