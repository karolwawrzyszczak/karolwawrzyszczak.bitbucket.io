<?php 
    require '../database.php';
    $indeks = null;
    if ( !empty($_GET['indeks'])) {
        $indeks = $_REQUEST['indeks'];
    }
     
    if ( null==$indeks ) {
        header("Location: ../lista_samochodzikow.php");
    } else {
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "SELECT * FROM samochodziki where id = ?";
        $q = $pdo->prepare($sql);
        $q->execute(array($indeks));
        $data = $q->fetch(PDO::FETCH_ASSOC);

        $sql = "SELECT * FROM dzieci WHERE id = '".$data['id_dziecka'] ."' ";
        foreach ($pdo->query($sql) as $imie) {
        }
        Database::disconnect();
    }
?>
 
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>

</head>
 
<body>
    <div class="container">
     
                <div class="span10 offset1">
                    <div class="row">
                        <h3>Dane posiadania samochodzika przez dziecko</h3>
                    </div>

                      <div class="control-group row">
                        <label class="col-sm-3 control-label">Imie</label>
                        <div class="col-sm-3">
                            <label class="form-control">
                                <?php echo $imie['imie']; echo ' '; echo $imie['nazwisko'] ; ?>
                            </label>
                        </div>
                      </div>
					  <div class="control-group row">
                        <label class=" col-sm-3 control-label">Model</label>
                        <div class="col-sm-3">
                            <label class="form-control">
                                <?php echo $data['model'];?>
                            </label>
                        </div>
                      </div>

                        <div class="form-actions">
                          <a class="btn btn-info" href="../lista_samochodzikow.php">Cofnij</a>
                       </div>
                     
                      
                    </div>
                </div>
                 
    </div> <!-- /container -->
  </body>
</html>
