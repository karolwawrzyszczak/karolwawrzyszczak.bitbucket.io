<?php 
     require '../database.php';
      if ( !empty($_POST)) {
        // inicjalizowanie wartości zmiennych kontroli poprawnosci wprowadzania 
		$id_dzieckaError = null;
		$modelError = null;


        // wartości tablicy POST
		$id_dziecka = $_POST['id_dziecka'];
		$model = $_POST['model'];

        
        // walidacja kolejnych zmiennych pól formularza
        $valid = true;
        //sprawdza czy dziecko w bazie

        if (empty($id_dziecka)) {
            $id_dzieckaError = 'wprowadź id dziecka';
            $valid = false;
        }
         if (empty($model)) {
          $modelError = 'wprowadź model samochodu';
            $valid = false;
        }

		// wprowadź dane
        if ($valid) {
			echo "ok- wprowadzenie";
            $pdo = Database::connect();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "INSERT INTO samochodziki (id_dziecka,model) values(?,?)";
            $q = $pdo->prepare($sql);
            $q->execute(array($id_dziecka,$model));
           // Database::disconnect();
            header("Location: ../lista_samochodzikow.php");
        }
    }
?>
<!DOCTYPE html>
<html lang="pl">

<head>
    <meta charset="UTF-8">
    <title>Dodawanie</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>

</head>
<body>
    <div class="container">
	<div class="span10 offset1">
        <div class="row">
            <h3>Dodaj samochodzik do dziecka</h3>
        </div>

        <form  action="create.php" method="post">
            <div class="form-group row" >
                <label  class="col-sm-1 control-label">Imię i Nazwisko</label>
			    <div class="col-sm-5">
                <?php
                    echo '<select name="id_dziecka">';
                    $pdo = Database::connect();
                    $sql = 'SELECT * FROM dzieci ';
                    foreach ($pdo->query($sql) as $row) {
                    echo '<option value=' . $row['id']. '>' . $row['imie']. ' '. $row['nazwisko']. '</option>';
                    }
                    echo '</select>';
                  ?>

				  <?php if (!empty($id_dzieckaError)): ?>
                  <span class="text-danger"><?php echo $id_dzieckaError;?></span>
                  <?php endif; ?>

			    </div>
            </div>
             <div class="form-group row">
                <label class="col-sm-1 control-label">Model samochodu</label>
			    <div class="col-sm-5">
                  <input name="model" type="text"  class="form-control" placeholder="wpisz model samochodu" value="<?php echo !empty($model)?$model:'';?>">
				  <?php if (!empty($modelError)): ?>
                                <span class="text-danger"><?php echo $modelError;?> </span>
                  <?php endif; ?>
			    </div>
            </div>   

            <div class="form-actions">
                <button type="submit" class="btn btn-success">Zapisz</button>
                <a class="btn btn-primary" href="../lista_samochodzikow.php">Cofnij</a>
            </div>
        </form>
      </div>           
    </div> <!-- /container -->
  </body>

</html>