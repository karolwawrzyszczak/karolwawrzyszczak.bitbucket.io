<?php 
    require '../database.php';
 
    $indeks = null;
    if ( !empty($_GET['indeks'])) {
        $indeks = $_REQUEST['indeks'];
    }

     
    if ( null==$indeks ) {
        header("Location: index.php");
    }
  
  if ( !empty($_POST)) {
    $id_dzieckaError = null;
    $modelError = null;


    // wartości tablicy POST
    $id_dziecka = $_POST['id_dziecka'];
    $model = $_POST['model'];


    // walidacja kolejnych zmiennych pól formularza
    $valid = true;
    if (empty($id_dziecka)) {
        $id_dzieckaError = 'wprowadź id dziecka';
        $valid = false;
    }
     if (empty($model)) {
      $modelError = 'wprowadź model samochodu';
        $valid = false;
    }

    // aktualizuj dane
    if ($valid) {
            $pdo = Database::connect();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "UPDATE samochodziki SET id_dziecka = ?, model = ?  WHERE id = ?";
            $q = $pdo->prepare($sql);
            $q->execute(array($id_dziecka,$model,$indeks));
            Database::disconnect();
            header("Location: ../lista_samochodzikow.php");
        }
    } else {
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "SELECT * FROM samochodziki where id = ?";
        $q = $pdo->prepare($sql);
        $q->execute(array($indeks));
        $data = $q->fetch(PDO::FETCH_ASSOC);
        $id_dziecka = $data['id_dziecka'];
        $model = $data['model'];

        Database::disconnect();
    }
?>

<!DOCTYPE html>
<html lang="pl">

<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>

</head>
<body>
    <div class="container">
	<div class="span10 offset1">
        <div class="row">
            <h3>Aktualizacja danych dziecka</h3>
        </div>
              
        <form  action="update.php?indeks=<?php echo $indeks?>" method="post">
            <div class="form-group row" >
                <label class="col-sm-1 control-label">id_dziecka</label>
                    <div class="col-sm-5">
                        <input name="id_dziecka" type="text"  class="form-control" placeholder="id_dziecka" value="<?php echo !empty($id_dziecka)?$id_dziecka:'';?>">
                            <?php if (!empty($id_dzieckaError)): ?>
                                <span class="text-danger"><?php echo $id_dzieckaError;?></span>
                            <?php endif; ?>
                     </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-1 control-label">Model</label>
                    <div class="col-sm-5">
                            <input name="model" type="text" class="form-control"  placeholder="model" value="<?php echo !empty($model)?$model:'';?>">
                            <?php if (!empty($modelError)): ?>
                                <span class="text-danger"><?php echo $modelError;?></span>
                            <?php endif; ?>
                        </div>
            </div>

            <div class="form-actions">
                <button type="submit" class="btn btn-success">Zapisz</button>
                <a class="btn btn-primary" href="../lista_samochodzikow.php">Cofnij</a>
            </div>
        </form>
      </div>           
    </div> <!-- /container -->
	
  </body>

</html>