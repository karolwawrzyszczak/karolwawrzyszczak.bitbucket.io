<?php 
    require '../database.php';
 
    $indeks = null;
    if ( !empty($_GET['indeks'])) {
        $indeks = $_REQUEST['indeks'];
    }

     
    if ( null==$indeks ) {
        header("Location: ../lista_dzieci.php");
    }
  
  if ( !empty($_POST)) {
  		$imieError = null;
          $nazwiskoError = null;
          $telError = null;

          // wartości tablicy POST
  		$imie = $_POST['imie'];
          $nazwisko = $_POST['nazwisko'];
          $tel= $_POST['tel'];

          // walidacja kolejnych zmiennych pól formularza
          $valid = true;
           if (empty($imie)) {
            $imieError = 'wprowadź imie dziecka';
              $valid = false;
          }
  		if (empty($nazwisko)) {
             $nazwiskoError = 'wprowadź nazwisko dziecka';
              $valid = false;
          }
        if (empty($tel)) {
           $telError = 'wprowadź nr telefonu';
            $valid = false;
       }else if ((strlen($tel) <= 8) || (strlen($tel) > 11)){
                $telError = 'wprowadź poprawny nr telefonu';
                $valid = false;
            }

    // aktualizuj dane
    if ($valid) {
            $pdo = Database::connect();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "UPDATE dzieci SET imie = ?, nazwisko = ?, rodzic_telefon = ? WHERE id = ?";
            $q = $pdo->prepare($sql);
            $q->execute(array($imie,$nazwisko,$tel,$indeks));
            Database::disconnect();
            header("Location: ../lista_dzieci.php");
        }
    } else {
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "SELECT * FROM dzieci where id = ?";
        $q = $pdo->prepare($sql);
        $q->execute(array($indeks));
        $data = $q->fetch(PDO::FETCH_ASSOC);
        $imie = $data['imie'];
        $nazwisko = $data['nazwisko'];
        $tel = $data['rodzic_telefon'];

        Database::disconnect();
    }
?>

<!DOCTYPE html>
<html lang="pl">

<head>
    <meta charset="UTF-8">
    <title>Aktualizuj</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>

</head>
<body>
    <div class="container">
	<div class="span10 offset1">
        <div class="row">
            <h3>Aktualizacja danych dziecka</h3>
        </div>
              
        <form  action="update.php?indeks=<?php echo $indeks?>" method="post">
            <div class="form-group row" >
                <label class="col-sm-1 control-label">Imie</label>
                    <div class="col-sm-5">
                        <input name="imie" type="text"  class="form-control" placeholder="imie" value="<?php echo !empty($imie)?$imie:'';?>">
                            <?php if (!empty($imieError)): ?>
                                <span class="text-danger"><?php echo $imieError;?></span>
                            <?php endif; ?>
                     </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-1 control-label">Nazwisko</label>
                    <div class="col-sm-5">
                            <input name="nazwisko" type="text" class="form-control"  placeholder="nazwisko" value="<?php echo !empty($nazwisko)?$nazwisko:'';?>">
                            <?php if (!empty($nazwiskoError)): ?>
                                <span class="text-danger"><?php echo $nazwiskoError;?></span>
                            <?php endif; ?>
                        </div>
            </div>

            <div class="form-group row">
                <label class=" col-sm-1 control-label">Nr telefonu</label>
                <div class="col-sm-5">
                  <input name="tel" type="text"  class="form-control" placeholder="wpisz nr telefonu" value="<?php echo !empty($tel)?$tel:'';?>">
                  <?php if (!empty($telError)): ?>
                                <span class="text-danger"><?php echo $telError;?> </span>
                  <?php endif; ?>
                </div>
            </div>

            <div class="form-actions">
                <button type="submit" class="btn btn-success">Zapisz</button>
                <a class="btn btn-primary" href="../lista_dzieci.php">Cofnij</a>
            </div>
        </form>
      </div>           
    </div> <!-- /container -->
	
  </body>

</html>