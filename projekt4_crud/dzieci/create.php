<?php 
     require '../database.php';
      if ( !empty($_POST)) {
        // inicjalizowanie wartości zmiennych kontroli poprawnosci wprowadzania 
		$imieError = null;
        $nazwiskoError = null;
        $telError = null;

        // wartości tablicy POST
		$imie = $_POST['imie'];
        $nazwisko = $_POST['nazwisko'];
        $tel= $_POST['nazwisko'];
        
        // walidacja kolejnych zmiennych pól formularza
        $valid = true;
         if (empty($imie)) {
          $imieError = 'wprowadź imie dziecka';
            $valid = false;
        }
		if (empty($nazwisko)) {
           $nazwiskoError = 'wprowadź nazwisko dziecka';
            $valid = false;
        }
        if (empty($tel)) {
           $telError = 'wprowadź nr telefonu';
            $valid = false;
       }else if (($tel > 8) && ($tel < 11)){
                $telError = 'wprowadź poprawny nr telefonu';
                $valid = false;
            }

		// wprowadź dane
        if ($valid) {
			echo "ok- wprowadzenie";
            $pdo = Database::connect();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "INSERT INTO dzieci (imie,nazwisko,rodzic_telefon) values(?,?,?)";
            $q = $pdo->prepare($sql);
            $q->execute(array($imie, $nazwisko,$tel));
            Database::disconnect();
            header("Location: ../lista_dzieci.php");
        }
    }
?>
<!DOCTYPE html>
<html lang="pl">

<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>

</head>
<body>
    <div class="container">
	<div class="span10 offset1">
        <div class="row">
            <h3>Wprowadź nowe dziecko</h3>
        </div>
              
        <form  action="create.php" method="post">

             <div class="form-group row">
                <label class="col-sm-1 control-label">Imię</label>
			    <div class="col-sm-5">
                  <input name="imie" type="text"  class="form-control" placeholder="wpisz imię dziecka" value="<?php echo !empty($imie)?$imie:'';?>">
				  <?php if (!empty($imieError)): ?>
                                <span class="text-danger"><?php echo $imieError;?> </span>
                  <?php endif; ?>
			    </div>
            </div>   
            <div class="form-group row">
                <label class=" col-sm-1 control-label">Nazwisko</label>
			    <div class="col-sm-5">
                  <input name="nazwisko" type="text"  class="form-control" placeholder="wpisz nazwisko dziecka" value="<?php echo !empty($nazwisko)?$nazwisko:'';?>">
				  <?php if (!empty($nazwiskoError)): ?>
                                <span class="text-danger"><?php echo $nazwiskoError;?> </span>
                  <?php endif; ?>
			    </div>
            </div>
            <div class="form-group row">
                <label class=" col-sm-1 control-label">Nr telefonu</label>
                <div class="col-sm-5">
                  <input name="tel" type="text"  class="form-control" placeholder="wpisz nr telefonu" value="<?php echo !empty($tel)?$tel:'';?>">
                  <?php if (!empty($telError)): ?>
                                <span class="text-danger"><?php echo $telError;?> </span>
                  <?php endif; ?>
                </div>
            </div>
            <div class="form-actions">
                <button type="submit" class="btn btn-success">Zapisz</button>
                <a class="btn btn-primary" href="../lista_dzieci.php">Cofnij</a>
            </div>
        </form>
      </div>           
    </div> <!-- /container -->
  </body>

</html>