<?php
 require 'database.php';

 function fetch_data()
 {
      $output = '';
        $pdo = Database::connect();
            $sql = ' SELECT samochodziki.id, model,id_dziecka, imie, nazwisko FROM samochodziki INNER JOIN dzieci ON samochodziki.id_dziecka = dzieci.id ORDER BY samochodziki.id ASC';
           foreach ($pdo->query($sql) as $row) {
              $output .= '<tr>
                  <td>'.$row["id"].'</td>
                  <td>'.$row["id_dziecka"].'</td>
                  <td>'.$row["imie"]. " " .$row ["nazwisko"] . '</td>
                  <td>'.$row["model"].'</td>
                 </tr>
                ';
            }

       Database::disconnect();
      return $output;
 }
 if(isset($_POST["create_pdf"]))
 {
      require_once('tcpdf/tcpdf.php');
      $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
      $obj_pdf->SetCreator(PDF_CREATOR);
      $obj_pdf->SetTitle("Raport PDF bazy samochodzików");
      $obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);
      $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
      $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
      $obj_pdf->SetDefaultMonospacedFont('helvetica');
      $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
      $obj_pdf->SetMargins(PDF_MARGIN_LEFT, '4', PDF_MARGIN_RIGHT);
      $obj_pdf->setPrintHeader(false);
      $obj_pdf->setPrintFooter(false);
      $obj_pdf->SetAutoPageBreak(TRUE, 10);
      $obj_pdf->SetFont('helvetica', '', 12);
      $obj_pdf->AddPage();
      $content = '';
      $content .= '
      <h3 align="center">Wyeksortowana baza samochodziki do dokumentu DPF</h3><br /><br />
      <table border="1" cellspacing="0" cellpadding="4">
           <tr>
                <th width="5%">ID</th>
                <th width="15%">ID_dziecka</th>
                <th width="30%">Imie i Nazwisko</th>
                <th width="45%">Model</th>
           </tr>
      ';
      $content .= fetch_data();
      $content .= '</table>';
      $obj_pdf->writeHTML($content);
       ob_end_clean();
      $obj_pdf->Output('sample.pdf', 'I');
 }

  if(isset($_POST["create_and_send"]))
  {
       require_once('tcpdf/tcpdf.php');
       $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
       $obj_pdf->SetCreator(PDF_CREATOR);
       $obj_pdf->SetTitle("Raport PDF bazy samochodzików");
       $obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);
       $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
       $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
       $obj_pdf->SetDefaultMonospacedFont('helvetica');
       $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
       $obj_pdf->SetMargins(PDF_MARGIN_LEFT, '4', PDF_MARGIN_RIGHT);
       $obj_pdf->setPrintHeader(false);
       $obj_pdf->setPrintFooter(false);
       $obj_pdf->SetAutoPageBreak(TRUE, 10);
       $obj_pdf->SetFont('helvetica', '', 12);
       $obj_pdf->AddPage();
       $content = '';
       $content .= '
       <h3 align="center">Export HTML Table data to PDF using TCPDF in PHP</h3><br /><br />
       <table border="1" cellspacing="0" cellpadding="4">
            <tr>
                 <th width="5%">ID</th>
                 <th width="15%">ID_dziecka</th>
                 <th width="30%">Imie i Nazwisko</th>
                 <th width="45%">Model</th>
            </tr>
       ';
       $content .= fetch_data();
       $content .= '</table>';
       $obj_pdf->writeHTML($content);
        ob_end_clean();

        $to='mohig57913@repshop.net';

        //sender
        $from = 'moderator@dzieciaczkow.com';
        $fromName = 'CodexWorld';

        //email subject
        $subject = 'PHP Email with Attachment by CodexWorld';

        //attachment file path
        $file= $obj_pdf->Output('sample.pdf', 'E');



        //email body content
        $htmlContent = '<h1>PHP Email with Attachment by CodexWorld</h1>
            <p>This email has sent from PHP script with attachment.</p>';

        //header for sender info
        $headers = "From: $fromName"." <".$from.">";

        //boundary
        $semi_rand = md5(time());
        $mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";

        //headers for attachment
        $headers .= "\nMIME-Version: 1.0\n" . "Content-Type: multipart/mixed;\n" . " boundary=\"{$mime_boundary}\"";

        //multipart boundary
        $message = "--{$mime_boundary}\n" . "Content-Type: text/html; charset=\"UTF-8\"\n" .
        "Content-Transfer-Encoding: 7bit\n\n" . $htmlContent . "\n\n";

        //preparing attachment
        if(!empty($file) > 0){
            if(is_file($file)){
                $message .= "--{$mime_boundary}\n";
                $fp =    @fopen($file,"rb");
                $data =  @fread($fp,filesize($file));

                @fclose($fp);
                $data = chunk_split(base64_encode($data));
                $message .= "Content-Type: application/octet-stream; name=\"".basename($file)."\"\n" .
                "Content-Description: ".basename($file)."\n" .
                "Content-Disposition: attachment;\n" . " filename=\"".basename($file)."\"; size=".filesize($file).";\n" .
                "Content-Transfer-Encoding: base64\n\n" . $data . "\n\n";
            }
        }
        $message .= "--{$mime_boundary}--";
        $returnpath = "-f" . $from;

        //send email
        $mail = @mail($to, $subject, $message, $headers, $returnpath);

        if(mail){
          echo"<script>  alert('EMAIL został wysłany!'); </script>";
       }


  }




 ?>
 <!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
           <title>PDF CREATOR</title>
              <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
               <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
               <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
               <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>

           <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
      </head>
      <body>
      <header><br>
          <div class="row justify-content-center">
              <a href="panel.php"><h1>---- COFNIJ -----</h1></a>

          </div>
      </header

           <br />
           <div class="container" style="width:700px;">
                <h3 align="center">Raport PDF bazy samochodzików</h3><br />
                <div class="table-responsive">
                     <table class="table table-bordered">
                          <tr>
                                <th width="5%">ID</th>
                                <th width="15%">ID_dziecka</th>
                                <th width="30%">Imie i Nazwisko</th>
                                <th width="45%">Model</th>
                          </tr>
                     <?php
                     echo fetch_data();
                     ?>
                     </table>
                     <br />

                     <form method="post">
                          <input type="submit" name="create_pdf" class="btn btn-danger" value="Generuj PDF" />
                     </form>
                      <form method="post">
                           <input type="submit" name="create_and_send" class="btn btn-danger" value="Generuj PDF i wyślij" />
                      </form>
                     <br><br>
                </div>
           </div>
      </body>
 </html>