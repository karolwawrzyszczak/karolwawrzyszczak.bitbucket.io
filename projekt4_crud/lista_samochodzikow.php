<html lang="pl">
<head>
    <meta charset="UTF-8">
    <title>Baza samochodzikow</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <?php SESSION_START();?>

</head>
<body>
<header><br>
    <div class="row justify-content-center">
        <a href="wys_samochodziki.php"><h1>---- COFNIJ -----</h1></a>

    </div>
</header><br><br>

<div class="container">
    <div class="row">
        <h3>PHP CRUD dla tabeli samochodziki</h3>
    </div>
     <p>
      <a href="samochodziki/create.php" class="btn btn-success">Dodaj samochodzik do dziecka</a>
     </p>

        <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>id</th>
            <th>id dziecka</th>
            <th>Imie Nazwisko</th>
            <th>model</th>
            <th>EDYCJA</th>
        </tr>
        </thead>
        <tbody>
        <?php
        require 'database.php';
                         $pdo = Database::connect();
            $sql = ' SELECT samochodziki.id, model,id_dziecka, imie, nazwisko FROM samochodziki INNER JOIN dzieci ON samochodziki.id_dziecka = dzieci.id ORDER BY samochodziki.id ASC';
           foreach ($pdo->query($sql) as $row) {

                echo '<tr>';
                echo '<td>'. $row['id'] . '</td>';
                echo '<td>'. $row['id_dziecka'].'</td>';
                echo '<td>'. $row['imie'].''; echo ' ' .$row ['nazwisko'] . '</td>';
                echo '<td>'. $row['model'] . '</td>';
                echo '<td><a class="btn btn-secondary" href="samochodziki/read.php?indeks='.$row['id'].'">Przeglądaj</a> ';
                echo '<a class="btn btn-success" href="samochodziki/update.php?indeks='.$row['id'].'">Aktualizuj</a> ';
                echo '<a class="btn btn-danger" href="samochodziki/delete.php?indeks='.$row['id'].'">Usuń</a></td>';
                echo '</tr>';
            Database::disconnect();
             }
        ?>

            </tbody>
        </table>
    </div>

</body>
</html>
